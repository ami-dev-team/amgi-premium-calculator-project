﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.API.Models
{
    public class TravelReq
    {
        public string TravelType { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Unit { get; set; }

    }
}
