﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.API.Models
{
    public class MainDBContext : DbContext
    {
        IConfiguration Configuration;
        public DbSet<TravelInsuranceType> TravelInsuranceType { get; set; }
        public DbSet<PremiumUnitAmount> PremiumUnitAmount { get; set; }

        public MainDBContext(DbContextOptions options) : base(options)
        {
            //...
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity("AMGI.PremiumCalculator.API.Models.PremiumUnitAmount", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<decimal>("Amount");

                b.Property<string>("CreatedBy");

                b.Property<DateTime>("CreatedDate");

                b.Property<int>("Days");

                b.Property<string>("Description");

                b.Property<string>("ModifiedBy");

                b.Property<DateTime>("ModifiedDate");

                b.Property<string>("Name");

                b.Property<string>("TravelInsuranceTypeCode");

                b.Property<Guid>("TravelInsuranceTypeID");

                b.HasKey("Id");

                b.ToTable("PremiumUnitAmount");
            });

            modelBuilder.Entity("AMGI.PremiumCalculator.API.Models.TravelInsuranceType", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<bool>("Active");

                b.Property<string>("Code");

                b.Property<string>("CreatedBy");

                b.Property<DateTime>("CreatedDate");

                b.Property<string>("Description");

                b.Property<string>("ModifiedBy");

                b.Property<DateTime>("ModifiedDate");

                b.Property<string>("Name");

                b.Property<string>("Term");

                b.HasKey("Id");

                b.ToTable("TravelInsuranceType");
            });
        }
    }
}
