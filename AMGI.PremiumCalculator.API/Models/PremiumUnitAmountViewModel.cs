﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.API.Models
{
    public class PremiumUnitAmountViewModel
    {
        public decimal PremiumAmount { get; set; }
        public decimal Coverage { get; set; }
    }
}
