﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMGI.PremiumCalculator.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AMGI.PremiumCalculator.API.Controllers
{
    [Produces("application/json")]
    [Route("Travel/[action]")]
    [ApiController]
    public class TravelController : ControllerBase
    {
        private readonly MainDBContext _db;

        public TravelController(MainDBContext db)
        {
            _db = db;
        }

        // GET: TravelInsuranceType
        [HttpGet]
        [ActionName("Get")]
        public IEnumerable<TravelInsuranceType> Get()
        {
            return _db.TravelInsuranceType;
        }

        [HttpPost]
        public IActionResult GetPremiumUnitAmount([FromBody] TravelReq model)
        {
            double coverAmount = 0.00;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.TravelType == "Domestic Travel" || model.TravelType == "Oversea Travel")
            {
                coverAmount = 500000;
            }

            DateTime fromDate = Convert.ToDateTime(model.FromDate);
            DateTime toDate = Convert.ToDateTime(model.ToDate);

            if (fromDate > toDate)
            {
                return BadRequest();
            }
            if(model.TravelType == "Oversea Travel")
            {
                int DateDiff = ((toDate - fromDate).Days) + 1;
                if(DateDiff < 7)
                {
                    return BadRequest();
                }
            }

            int totalDays = ((toDate - fromDate).Days) + 1;
            var DaysList = (from t1 in _db.PremiumUnitAmount
                            join t2 in _db.TravelInsuranceType on t1.TravelInsuranceTypeID equals t2.Id
                            where t2.Name == model.TravelType
                            orderby t1.Days ascending
                            select t1).ToList();

            for (int i = 0; i <= DaysList.Count(); i++)
            {
                if (totalDays <= DaysList[i].Days)
                {
                    totalDays = DaysList[i].Days;
                    break;
                }
            }

            var Amount = (from t1 in DaysList
                          where t1.Days == totalDays
                          select t1.Amount).FirstOrDefault();

            var PremiumUnitAomunt = (Convert.ToDecimal(Amount) * model.Unit);
            var totalCoverAmount = (Convert.ToDecimal(coverAmount) * model.Unit);

            PremiumUnitAmountViewModel PUAmount = new PremiumUnitAmountViewModel();
            PUAmount.PremiumAmount = PremiumUnitAomunt;
            PUAmount.Coverage = Convert.ToDecimal(totalCoverAmount.ToString("0.00"));

            return Ok(PUAmount);
        }
    }
}