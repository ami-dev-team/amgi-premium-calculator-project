﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.Project.Models
{
    public class PremiumUnitAmountViewModel
    {
        public Guid Id { get; set; }
        public Guid TravelInsuranceTypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Days { get; set; }
        public Decimal Amount { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string TravelInsuranceTypeName { get; set; }
    }
}
