﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.Project.Models
{
    public class LoginAttempt
    {
        [Key]
        public Guid TransId { get; set; }
        public Guid UserId { get; set; }
        public int? Attempt { get; set; }
        public string Status { get; set; }
        public DateTime? LoginDate { get; set; }
        public string UserName { get; set; }
        public bool? TransStatus { get; set; }
    }
}
