﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.Project.Models
{
    public class TravelReq
    {
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string TravelType { get; set; }
        public int Unit { get; set; }

        //public decimal PremiumAmount { get; set; }
        //public decimal Coverage { get; set; }
    }

}
