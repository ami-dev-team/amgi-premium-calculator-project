﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.Project.Models
{
    public class SysUser
    {
        public Guid Id { get; set; }
        public Guid Role { get; set; }
        public string Name { get; set; }
        public string Nrc { get; set; }
        public DateTime? Dob { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public Guid Branch { get; set; }
        public bool IsFirstTimeLogin { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public Guid Department { get; set; }
        public string Position { get; set; }
        public string Organization { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Remark { get; set; }
        public string AccountStatus { get; set; }
        public string ConfirmCode { get; set; }
        public int? Attempt { get; set; }
        public bool IsActive { get; set; }
       
    }
}
