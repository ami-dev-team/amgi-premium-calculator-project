﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.Project.Models
{
    public class TravelPremiumUnitAmountViewModel
    {
        public decimal PremiumAmount { get; set; }
        public decimal Coverage { get; set; }
    }
}
