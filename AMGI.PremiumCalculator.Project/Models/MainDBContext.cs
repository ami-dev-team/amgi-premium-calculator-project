﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.Project.Models
{
    public class MainDBContext : DbContext
    {
        IConfiguration Configuration;
        public DbSet<TravelInsuranceType> TravelInsuranceType { get; set; }
        public DbSet<PremiumUnitAmount> PremiumUnitAmount { get; set; }
        public DbSet<Branch> Branch { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Department> Department { get; set;}
        public DbSet<SysUser> SysUser { get; set; }
        public DbSet<AspNetUsers> AspNetUser { get; set; }
        public DbSet<LoginAttempt> LoginAttempt { get; set; }
        public DbSet<Product> Product { get; set; }

        public MainDBContext()
        {
            var builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }

        public MainDBContext(DbContextOptions options)
        : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity("AMGI.PremiumCalculator.Project.Models.PremiumUnitAmount", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<decimal>("Amount");

                b.Property<string>("CreatedBy");

                b.Property<DateTime>("CreatedDate");

                b.Property<int>("Days");

                b.Property<string>("Description");

                b.Property<string>("ModifiedBy");

                b.Property<DateTime>("ModifiedDate");

                b.Property<string>("Name");

                b.Property<string>("TravelInsuranceTypeCode");

                b.Property<Guid>("TravelInsuranceTypeID");

                b.HasKey("Id");

                b.ToTable("PremiumUnitAmount");
            });

            modelBuilder.Entity("AMGI.PremiumCalculator.Project.Models.TravelInsuranceType", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<bool>("Active");

                b.Property<string>("Code");

                b.Property<string>("CreatedBy");

                b.Property<DateTime>("CreatedDate");

                b.Property<string>("Description");

                b.Property<string>("ModifiedBy");

                b.Property<DateTime>("ModifiedDate");

                b.Property<string>("Name");

                b.Property<string>("Term");

                b.HasKey("Id");

                b.ToTable("TravelInsuranceType");
            });

            modelBuilder.Entity("AMGI.PremiumCalculator.Project.Models.AspNetUsers", b =>
            {
                b.Property<string>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<int>("AccessFailedCount");

                b.Property<string>("ConcurrencyStamp");

                b.Property<string>("Email");

                b.Property<bool>("EmailConfirmed");

                b.Property<bool>("LockoutEnabled");

                b.Property<DateTimeOffset?>("LockoutEnd");

                b.Property<string>("NormalizedEmail");

                b.Property<string>("NormalizedUserName");

                b.Property<string>("PasswordHash");

                b.Property<string>("PhoneNumber");

                b.Property<bool>("PhoneNumberConfirmed");

                b.Property<string>("SecurityStamp");

                b.Property<bool>("TwoFactorEnabled");

                b.Property<string>("UserName");

                b.HasKey("Id");

                b.ToTable("AspNetUser");
            });

            modelBuilder.Entity("AMGI.PremiumCalculator.Project.Models.Branch", b =>
            {
                b.Property<Guid>("BranchId")
                    .ValueGeneratedOnAdd();

                b.Property<bool?>("Active");

                b.Property<string>("Address");

                b.Property<string>("Description");

                b.Property<string>("Name");

                b.Property<string>("Phone");

                b.HasKey("BranchId");

                b.ToTable("Branch");
            });

            modelBuilder.Entity("AMGI.PremiumCalculator.Project.Models.Department", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<string>("Description");

                b.Property<string>("Name");

                b.HasKey("Id");

                b.ToTable("Department");
            });

            modelBuilder.Entity("AMGI.PremiumCalculator.Project.Models.Role", b =>
            {
                b.Property<Guid>("RoleId")
                    .ValueGeneratedOnAdd();

                b.Property<string>("Description");

                b.Property<string>("RoleName");

                b.HasKey("RoleId");

                b.ToTable("Role");
            });

            modelBuilder.Entity("AMGI.PremiumCalculator.Project.Models.SysUser", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<string>("AccountStatus");

                b.Property<string>("Address");

                b.Property<int?>("Attempt");

                b.Property<Guid>("Branch");

                b.Property<string>("ConfirmCode");

                b.Property<string>("CreatedBy");

                b.Property<DateTime>("CreatedDate");

                b.Property<Guid>("Department");

                b.Property<DateTime?>("Dob");

                b.Property<string>("Email");

                b.Property<bool>("IsActive");

                b.Property<bool>("IsFirstTimeLogin");

                b.Property<string>("Name");

                b.Property<string>("Nrc");

                b.Property<string>("Organization");

                b.Property<string>("Password");

                b.Property<string>("PasswordSalt");

                b.Property<string>("Phone");

                b.Property<string>("Position");

                b.Property<string>("Remark");

                b.Property<Guid>("Role");

                b.Property<string>("UpdatedBy");

                b.Property<DateTime?>("UpdatedDate");

                b.HasKey("Id");

                b.ToTable("SysUser");
            });

            modelBuilder.Entity("AMGI.PremiumCalculator.Project.Models.LoginAttempt", b =>
            {
                b.Property<Guid>("TransId")
                    .ValueGeneratedOnAdd();

                b.Property<int?>("Attempt");

                b.Property<DateTime?>("LoginDate");

                b.Property<string>("Status");

                b.Property<bool?>("TransStatus");

                b.Property<Guid>("UserId");

                b.Property<string>("UserName");

                b.HasKey("TransId");

                b.ToTable("LoginAttempt");
            });

            modelBuilder.Entity("AMGI.PremiumCalculator.Project.Models.Product", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<bool>("Active");

                b.Property<string>("CreatedBy");

                b.Property<DateTime>("CreatedDate");

                b.Property<string>("Description");

                b.Property<string>("ModifiedBy");

                b.Property<DateTime>("ModifiedDate");

                b.Property<string>("Name");

                b.HasKey("Id");

                b.ToTable("Product");
            });
        }
    }
}
