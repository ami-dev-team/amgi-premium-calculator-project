﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.PremiumCalculator.Project.Models
{
    public class SysUserViewModel
    {
        public Guid Id { get; set; }
        public Guid Role { get; set; }
        public string Name { get; set; }
        public string Nrc { get; set; }
        public DateTime? Dob { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public Guid Branch { get; set; }
        public bool IsFirstTimeLogin { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public Guid Department { get; set; }
        public string Position { get; set; }
        public string Organization { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Remark { get; set; }
        public string AccountStatus { get; set; }
        public string ConfirmCode { get; set; }
        public int? Attempt { get; set; }
        public bool IsActive { get; set; }
        public bool Health { get; set; }
        public bool Risk { get; set; }
        public bool SMS { get; set; }
        public bool UserRating { get; set; }
        public bool AccessLog { get; set; }
        public bool Claim { get; set; }

        #region for Risk Assessment
        public bool Motor { get; set; }
        public bool Fire { get; set; }
        public bool Marine { get; set; }
        #endregion


    }
}
