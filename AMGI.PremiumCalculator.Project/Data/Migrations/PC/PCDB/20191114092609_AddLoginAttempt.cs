﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AMGI.PremiumCalculator.Project.Data.Migrations.PC.PCDB
{
    public partial class AddLoginAttempt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LoginAttempt",
                columns: table => new
                {
                    TransId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Attempt = table.Column<int>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    LoginDate = table.Column<DateTime>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    TransStatus = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginAttempt", x => x.TransId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoginAttempt");
        }
    }
}
