﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AMGI.PremiumCalculator.Project.Data.Migrations.PC.PCDB
{
    public partial class ChangeDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PremiumUnitAmount",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TravelInsuranceTypeID = table.Column<Guid>(nullable: false),
                    TravelInsuranceTypeCode = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Days = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PremiumUnitAmount", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PremiumUnitAmount");
        }
    }
}
