#pragma checksum "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c4341497df1509ae28e61aa5fa6efd602370b6e9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_TravelSetup_CreatePremiumUnitAmount), @"mvc.1.0.view", @"/Views/TravelSetup/CreatePremiumUnitAmount.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/TravelSetup/CreatePremiumUnitAmount.cshtml", typeof(AspNetCore.Views_TravelSetup_CreatePremiumUnitAmount))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\_ViewImports.cshtml"
using AMGI.PremiumCalculator.Project;

#line default
#line hidden
#line 2 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\_ViewImports.cshtml"
using AMGI.PremiumCalculator.Project.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c4341497df1509ae28e61aa5fa6efd602370b6e9", @"/Views/TravelSetup/CreatePremiumUnitAmount.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2bb556fc4d372fe1211736123a7ef1abfdf8cee7", @"/Views/_ViewImports.cshtml")]
    public class Views_TravelSetup_CreatePremiumUnitAmount : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<AMGI.PremiumCalculator.Project.Models.PremiumUnitAmount>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/RoleList.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
  
    ViewData["Title"] = "CreatePremiumUnitAmount";
    Layout = "~/Views/Shared/_ManageLayout.cshtml";

#line default
#line hidden
            BeginContext(176, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
#line 8 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
 using (Html.BeginForm("SavePremiumUnitAmount", "TravelSetup", FormMethod.Post, new { enctype = "multipart/form-data" }))
{


#line default
#line hidden
            BeginContext(308, 204, true);
            WriteLiteral("    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <h3 class=\"page-header\">Travel Insurance Type</h3>\r\n        </div>\r\n        <div style=\"float:right;margin-right:30px;\">\r\n            <a");
            EndContext();
            BeginWriteAttribute("href", " href=\'", 512, "\'", 575, 1);
#line 16 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
WriteAttributeValue("", 519, Url.Action("PremiumUnitAmountList","TravelSetup", null), 519, 56, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(576, 36, true);
            WriteLiteral(" id=\"imgUserList\">\r\n                ");
            EndContext();
            BeginContext(612, 35, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "c4341497df1509ae28e61aa5fa6efd602370b6e95413", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(647, 50, true);
            WriteLiteral("\r\n            </a>\r\n\r\n        </div>\r\n    </div>\r\n");
            EndContext();
            BeginContext(704, 33, false);
#line 23 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
Write(Html.HiddenFor(model => model.Id));

#line default
#line hidden
            EndContext();
            BeginContext(741, 49, true);
            WriteLiteral("    <div class=\"row\">\r\n        <font color=\"red\">");
            EndContext();
            BeginContext(791, 19, false);
#line 26 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
                     Write(TempData["Message"]);

#line default
#line hidden
            EndContext();
            BeginContext(810, 22, true);
            WriteLiteral(" </font>\r\n    </div>\r\n");
            EndContext();
            BeginContext(834, 188, true);
            WriteLiteral("    <div class=\"row\">\r\n        <div class=\"form-horizontal\" style=\"margin-left:30px;\">\r\n\r\n            <div class=\"form-group\">\r\n                <div class=\"col-md-3\">\r\n                    ");
            EndContext();
            BeginContext(1023, 58, false);
#line 34 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.Label("Name", null, new { @class = "control-label" }));

#line default
#line hidden
            EndContext();
            BeginContext(1081, 86, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    ");
            EndContext();
            BeginContext(1168, 115, false);
#line 37 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.TextBoxFor(model => model.Name, new { @class = "form-control", @required = "required", autocomplete = "off" }));

#line default
#line hidden
            EndContext();
            BeginContext(1283, 146, true);
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                <div class=\"col-md-3\">\r\n                    ");
            EndContext();
            BeginContext(1430, 65, false);
#line 43 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.Label("Description", null, new { @class = "control-label" }));

#line default
#line hidden
            EndContext();
            BeginContext(1495, 86, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    ");
            EndContext();
            BeginContext(1582, 122, false);
#line 46 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.TextBoxFor(model => model.Description, new { @class = "form-control", @required = "required", autocomplete = "off" }));

#line default
#line hidden
            EndContext();
            BeginContext(1704, 146, true);
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                <div class=\"col-md-3\">\r\n                    ");
            EndContext();
            BeginContext(1851, 58, false);
#line 52 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.Label("Days", null, new { @class = "control-label" }));

#line default
#line hidden
            EndContext();
            BeginContext(1909, 86, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    ");
            EndContext();
            BeginContext(1996, 115, false);
#line 55 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.TextBoxFor(model => model.Days, new { @class = "form-control", @required = "required", autocomplete = "off" }));

#line default
#line hidden
            EndContext();
            BeginContext(2111, 146, true);
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                <div class=\"col-md-3\">\r\n                    ");
            EndContext();
            BeginContext(2258, 60, false);
#line 61 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.Label("Amount", null, new { @class = "control-label" }));

#line default
#line hidden
            EndContext();
            BeginContext(2318, 86, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    ");
            EndContext();
            BeginContext(2405, 117, false);
#line 64 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.TextBoxFor(model => model.Amount, new { @class = "form-control", @required = "required", autocomplete = "off" }));

#line default
#line hidden
            EndContext();
            BeginContext(2522, 146, true);
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                <div class=\"col-md-3\">\r\n                    ");
            EndContext();
            BeginContext(2669, 75, false);
#line 70 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.Label("Travel Insurance Type", null, new { @class = "control-label" }));

#line default
#line hidden
            EndContext();
            BeginContext(2744, 66, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n");
            EndContext();
            BeginContext(2971, 20, true);
            WriteLiteral("                    ");
            EndContext();
            BeginContext(2992, 158, false);
#line 74 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
               Write(Html.DropDownListFor(model => model.TravelInsuranceTypeCode, ViewBag.TravelInsuranceType as IEnumerable<SelectListItem>, " ", new { @class = "form-control" }));

#line default
#line hidden
            EndContext();
            BeginContext(3150, 284, true);
            WriteLiteral(@"
                </div>
            </div>

            <div class=""col-md-3"">

            </div>
            <div class=""col-md-4"">
                <button type=""submit"" class=""btn"" style=""color:#A92028;"">Save</button>
            </div>

        </div>


    </div>
");
            EndContext();
#line 89 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\CreatePremiumUnitAmount.cshtml"
    
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<AMGI.PremiumCalculator.Project.Models.PremiumUnitAmount> Html { get; private set; }
    }
}
#pragma warning restore 1591
