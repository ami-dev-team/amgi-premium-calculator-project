#pragma checksum "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "bc191a9d788207ca086e440be55819b4e4227319"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_TravelSetup_TravelInsuranceType), @"mvc.1.0.view", @"/Views/TravelSetup/TravelInsuranceType.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/TravelSetup/TravelInsuranceType.cshtml", typeof(AspNetCore.Views_TravelSetup_TravelInsuranceType))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\_ViewImports.cshtml"
using AMGI.PremiumCalculator.Project;

#line default
#line hidden
#line 2 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\_ViewImports.cshtml"
using AMGI.PremiumCalculator.Project.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bc191a9d788207ca086e440be55819b4e4227319", @"/Views/TravelSetup/TravelInsuranceType.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2bb556fc4d372fe1211736123a7ef1abfdf8cee7", @"/Views/_ViewImports.cshtml")]
    public class Views_TravelSetup_TravelInsuranceType : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<AMGI.PremiumCalculator.Project.Models.TravelInsuranceType>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/Edit.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/remove.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
  
    ViewData["Title"] = "TravelInsuranceType";
    Layout = "~/Views/Shared/_ManageLayout.cshtml";

#line default
#line hidden
            BeginContext(174, 317, true);
            WriteLiteral(@"
<div class=""row"">
    <div class=""col-lg-12"">
        <h3 class=""page-header"">
            Travel Insurance Type List
        </h3>
    </div>
</div>

<div class=""row"">
    <ul class=""nav navbar-right panel_toolbox"" style=""margin-right:20px;"">
        <li style=""float:left;display:none;"">
            <a");
            EndContext();
            BeginWriteAttribute("href", " href=\'", 491, "\'", 559, 1);
#line 18 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
WriteAttributeValue("", 498, Url.Action("CreateTravelInsuranceType", "TravelSetup", null), 498, 61, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(560, 392, true);
            WriteLiteral(@"><i class=""fa fa-plus"" style=""font-size:30px;""></i></a>
        </li>

        <li class=""dropdown"" style=""float:left;"">
            <a href=""#"" class=""dropdown-toggle"" data-toggle=""dropdown"" role=""button"" aria-expanded=""false""><i class=""fa fa-file-excel-o"" style=""font-size:30px;""></i></a>
            <ul class=""dropdown-menu"" role=""menu"">
                <li>
                    <a");
            EndContext();
            BeginWriteAttribute("href", " href=\'", 952, "\'", 1024, 1);
#line 25 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
WriteAttributeValue("", 959, Url.Action("ExportTravelInsuranceTypeList", "TravelSetup", null), 959, 65, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1025, 111, true);
            WriteLiteral("><i class=\"fa fa-download\"></i> Export</a>\r\n                </li>\r\n                <li>\r\n                    <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1136, "\"", 1202, 1);
#line 28 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
WriteAttributeValue("", 1143, Url.Action("ImportTravelInsuranceTypeList", "TravelSetup"), 1143, 59, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1203, 839, true);
            WriteLiteral(@" id=""btnImport""><i class=""fa fa-upload""></i> Import</a>
                </li>
            </ul>
        </li>

    </ul>
    <div class=""clearfix""></div>
</div>

<div class=""row"">
    <div class=""col-lg-12"">
        <div class=""panel panel-default"">
            <div class=""panel-body"">
                <table style=""width: 100%;"" class=""table table-striped table-bordered table-hover"" id=""policy-datatable"">
                    <thead>
                        <tr>
                            <th>CODE</th>
                            <th>NAME</th>
                            <th>DESCRIPTION</th>
                            <th>TERM</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
");
            EndContext();
#line 53 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
                         foreach (var traveltype in (List<TravelInsuranceType>)ViewBag.TravelInsuranceTypesList)
                        {

#line default
#line hidden
            BeginContext(2183, 81, true);
            WriteLiteral("                        <tr class=\"odd gradeX\">\r\n                            <td>");
            EndContext();
            BeginContext(2265, 15, false);
#line 56 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
                           Write(traveltype.Code);

#line default
#line hidden
            EndContext();
            BeginContext(2280, 39, true);
            WriteLiteral("</td>\r\n                            <td>");
            EndContext();
            BeginContext(2320, 15, false);
#line 57 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
                           Write(traveltype.Name);

#line default
#line hidden
            EndContext();
            BeginContext(2335, 39, true);
            WriteLiteral("</td>\r\n                            <td>");
            EndContext();
            BeginContext(2375, 22, false);
#line 58 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
                           Write(traveltype.Description);

#line default
#line hidden
            EndContext();
            BeginContext(2397, 39, true);
            WriteLiteral("</td>\r\n                            <td>");
            EndContext();
            BeginContext(2437, 15, false);
#line 59 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
                           Write(traveltype.Term);

#line default
#line hidden
            EndContext();
            BeginContext(2452, 102, true);
            WriteLiteral("</td>\r\n                            <td style=\"text-align:center;\">\r\n                                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\'", 2554, "\'", 2643, 1);
#line 61 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
WriteAttributeValue("", 2561, Url.Action("EditTravelInsuranceType", "TravelSetup", new { Id = @traveltype.Id }), 2561, 82, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2644, 39, true);
            WriteLiteral(">\r\n                                    ");
            EndContext();
            BeginContext(2683, 31, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "bc191a9d788207ca086e440be55819b4e422731910734", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2714, 170, true);
            WriteLiteral("\r\n                                </a>\r\n                            </td>\r\n                            <td style=\"text-align:center;\">\r\n                                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\'", 2884, "\'", 2981, 1);
#line 66 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
WriteAttributeValue("", 2891, Url.Action("DeleteTravelInsuranceTypeAsync", "TravelSetup", new { @Id = @traveltype.Id }), 2891, 90, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2982, 111, true);
            WriteLiteral(" onclick=\"return confirm(\'Are you sure you want to delete this item?\');\">\r\n                                    ");
            EndContext();
            BeginContext(3093, 33, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "bc191a9d788207ca086e440be55819b4e422731912734", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3126, 108, true);
            WriteLiteral("\r\n                                </a>\r\n                            </td>\r\n\r\n                        </tr>\r\n");
            EndContext();
#line 72 "D:\Development\PremiumCalculator\AMGI.PremiumCalculator.Project\AMGI.PremiumCalculator.Project\Views\TravelSetup\TravelInsuranceType.cshtml"
                        }

#line default
#line hidden
            BeginContext(3261, 218, true);
            WriteLiteral("                    </tbody>\r\n                    <tfoot>\r\n                    </tfoot>\r\n                </table>\r\n                <!-- /.table-responsive -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<AMGI.PremiumCalculator.Project.Models.TravelInsuranceType> Html { get; private set; }
    }
}
#pragma warning restore 1591
