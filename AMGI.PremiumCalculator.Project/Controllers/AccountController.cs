﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMGI.PremiumCalculator.Project.Areas.Identity.Pages.Account;
using AMGI.PremiumCalculator.Project.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;

namespace AMGI.PremiumCalculator.Project.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<LoginModel> _logger;
        public ISession session;
        MainDBContext _db = new MainDBContext();
        public AccountController(SignInManager<IdentityUser> signInManager, ILogger<LoginModel> logger, UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult ForgotPassword() //Change Password(Show Change Password Form)
        {
            return View();
        }

        public IActionResult AllLockUserList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var alllockuserlist = _db.SysUser.Where(u => u.AccountStatus == "Lock").ToList();
                return View(alllockuserlist);
            }
        }

        [Route("UnlockUser/{userId}")]
        public IActionResult LockUser(Guid userId)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                ViewBag.RoleList = new SelectList((from r in _db.Role
                                                   select r).ToList(), "RoleId", "Description");
                SysUser lockuser = _db.SysUser.Where(u => u.Id == userId).FirstOrDefault();
                return View(lockuser);
            }
        }

        [HttpPost]
        public IActionResult UnlockUser(SysUser user)
        {
            var unlockuser = _db.SysUser.Where(u => u.Id == user.Id).FirstOrDefault();
            unlockuser.Attempt = 1;
            unlockuser.AccountStatus = "Success";

            _db.Database.ExecuteSqlCommand("Update LoginAttempt set TransStatus = 0 Where UserID = '" + user.Id + "'");

            _db.SysUser.Update(unlockuser);
            _db.SaveChanges();
            return RedirectToAction("AllLockUserList", "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(AMGI.PremiumCalculator.Project.Areas.Identity.Pages.Account.LoginModel.InputModel model, string returnUrl = null)
        {
            session = HttpContext.Session;
            ViewData["ReturnUrl"] = returnUrl;
            returnUrl = returnUrl ?? Url.Content("~/");
            SysUser user = _db.SysUser.Where(u => u.Email == model.Email).FirstOrDefault();
            if (user != null)
            {
                var dept = _db.Department.Where(d => d.Id == user.Department).Select(d => d.Name).FirstOrDefault();
                var branch = _db.Branch.Where(b => b.BranchId == user.Branch).Select(b => b.Description).FirstOrDefault();
                var role = _db.Role.Where(r => r.RoleId == user.Role).Select(r => r.Description).FirstOrDefault();

                LoginAttempt loginAttempt = new LoginAttempt();
                if (user.AccountStatus != "Lock")
                {
                    if (ModelState.IsValid)
                    {
                        // This doesn't count login failures towards account lockout
                        // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                        var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                        if (result.Succeeded)
                        {
                            HttpContext.Session.SetString("userId", Convert.ToString(user.Id));
                            HttpContext.Session.SetString("username", user.Name);
                            session.SetString("position", user.Position);
                            HttpContext.Session.SetString("email", user.Email);
                            session.SetString("department", (dept == "IT") ? "1" : "0");
                            session.SetString("branch", branch);
                            session.SetString("role", role);

                            if (user.IsFirstTimeLogin == true)
                            {
                                //return RedirectToAction("Index", "Manage");
                                return Redirect("~/Identity/Account/Manage/ChangePassword");
                            }
                            else
                            {

                                _logger.LogInformation("User logged in.");
                                return Redirect(returnUrl);
                                //return RedirectToAction("Index","Home");
                            }

                        }

                        if (result.RequiresTwoFactor)
                        {
                            session.SetString("userId", Convert.ToString(user.Id));
                            session.SetString("username", user.Name);
                            session.SetString("position", user.Position);
                            session.SetString("email", user.Email);
                           // session.SetString("department", (dept == "IT") ? "1" : "0");
                            session.SetString("branch", branch);
                            session.SetString("role", role);

                            //return RedirectToAction(nameof(LoginWith2fa), new { returnUrl, model.RememberMe });
                            return Redirect("~/Identity/Account/LoginWith2fa");
                        }
                        if (result.IsLockedOut)
                        {
                            _logger.LogWarning("User account locked out.");
                            //return RedirectToAction(nameof(Lockout));
                            return Redirect("~/Identity/Account/Lockout");
                        }

                        else
                        {
                            loginAttempt.TransId = Guid.NewGuid();
                            loginAttempt.UserId = user.Id;
                            loginAttempt.Status = "Fail";
                            loginAttempt.Attempt = 1;
                            loginAttempt.LoginDate = System.DateTime.Now;
                            loginAttempt.UserName = user.Name;
                            loginAttempt.TransStatus = true;

                            _db.Entry(loginAttempt).State = EntityState.Added;
                            _db.SaveChanges();

                            var FailCount = _db.LoginAttempt.Where(r => r.UserId == user.Id && r.Status == "Fail" && r.LoginDate.Value.Date == DateTime.Today && r.TransStatus == true).Count();

                            //User locked
                            if (FailCount >= 3)
                            {
                                user.AccountStatus = "Lock";
                                user.Attempt = 3;
                                _db.Entry(user).State = EntityState.Modified;
                                _db.SaveChanges();
                            }
                            //If user password wrong, assign data to attempt 
                            else
                            {
                                user.Attempt = 2;
                                user.AccountStatus = "Fail";
                                _db.Entry(user).State = EntityState.Modified;
                                _db.SaveChanges();
                            }

                            ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                            return View(model);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Your account is locked,please contact to administrator.");
                    return View(model);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "You are not registerd. Please contact to administrator.");
                return View(model);
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ResetNewPassword(SysUser model)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist
                    //return RedirectToAction(nameof(ResetPasswordConfirmation));
                    return RedirectToPage("./ResetPasswordConfirmation");
                }
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var result = await _userManager.ResetPasswordAsync(user, code, "Ami@123");
                if (result.Succeeded)
                {
                    SysUser appUser = _db.SysUser.Where(u => u.Email == user.Email).FirstOrDefault();
                    appUser.IsFirstTimeLogin = true;
                    _db.SysUser.Update(appUser);
                    _db.SaveChanges();

                    return RedirectToAction("UserList", "Admin");
                }
               // AddErrors(result);
                return View();
            }
        }

        public IActionResult ExportLockUserList()
        {
            var userlist = (from t1 in _db.SysUser
                            join t2 in _db.Role on t1.Role equals t2.RoleId
                            where (t1.AccountStatus == "Lock")
                            select new SysUser
                            {
                                Name = t1.Name,
                                Address = t2.Description,
                                Email = t1.Email,
                                Phone = t1.Phone,
                                Organization = t1.Organization,
                                //Address = t1.Address
                            }
                            ).ToList();


            var columnHeader = new List<string>
            {
                "Name",
                "Role",
                "Email",
                "Phone",
                "Organization"
                //"Address"

            };

            var fileContent = ExportExcelForLockUserList(userlist, columnHeader, "Test");
            return File(fileContent, "application/ms-excel", "UserLockList.xlsx");
        }

        public byte[] ExportExcelForLockUserList(List<SysUser> userList, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in userList)
                {
                    worksheet.Cells["A" + j].Value = item.Name;
                    worksheet.Cells["B" + j].Value = item.Address;
                    worksheet.Cells["C" + j].Value = item.Email;
                    worksheet.Cells["D" + j].Value = item.Phone;
                    worksheet.Cells["E" + j].Value = item.Organization;
                   // worksheet.Cells["F" + j].Value = item.Address1;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }


    }
}