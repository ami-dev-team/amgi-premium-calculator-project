﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AMGI.PremiumCalculator.Project.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;

namespace AMGI.PremiumCalculator.Project.Controllers
{
    public class TravelSetupController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public ISession session;
        MainDBContext _db = new MainDBContext();
        public TravelSetupController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                return View();
            }
        }

        #region Travel Insurance Type

        public IActionResult TravelInsuranceType()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                TravelInsuranceType travelInsuranceType = new TravelInsuranceType();
                var travelInsuranceTypesList = _db.TravelInsuranceType.ToList();
                ViewBag.TravelInsuranceTypesList = travelInsuranceTypesList;
                return View();
            }
        }

        public IActionResult CreateTravelInsuranceType(TravelInsuranceType travelInsuranceType)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                return View();
            }
        }

        [HttpPost]
        public IActionResult SaveTravelInsuranceType(TravelInsuranceType model)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                try
                {
                    if (model.Id == Guid.Empty)
                    {
                        model.Id = Guid.NewGuid();
                        model.CreatedDate = DateTime.Now;
                        model.ModifiedDate = DateTime.Now;
                        session = HttpContext.Session;
                        string username = session.GetString("username");
                        model.CreatedBy = username;
                        model.ModifiedBy = username;

                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();

                        ViewBag.Message = "Save Successful.";
                        return RedirectToAction("TravelInsuranceType");

                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            session = HttpContext.Session;
                            model.ModifiedBy = session.GetString("username");
                            model.ModifiedDate = DateTime.Now;

                            _db.Update(model);
                            _db.SaveChanges();

                            ViewBag.Message = "Save Successful.";
                        }
                    }
                }
                catch (DbUpdateException)
                {
                    TempData["Message"] = "Error in Saving Process.";
                    return RedirectToAction("CreateTravelInsuranceType", model);
                    //throw ex;
                }
            }
            TempData["Message"] = "Save Successful";
            return RedirectToAction("TravelInsuranceType");       
        }

        public IActionResult ImportTravelInsuranceTypeList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                return View();
            }
        }

        [HttpPost]
        [Route("ImportTravelInsuranceType")]
        public IActionResult ImportExcelData(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                string folderName = "ImportedFiles";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                // Delete Files from Directory
                System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                foreach (FileInfo filesDelete in di.GetFiles())
                {
                    filesDelete.Delete();
                }// End Deleting files form directories

                if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                {
                    Directory.CreateDirectory(newPath);
                }
                var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                {
                    reportfile.CopyTo(fileStream);
                }
                // Get uploaded file path with root
                string rootFolder = _hostingEnvironment.WebRootPath;
                string fileName = @"ImportedFiles/" + fiName;
                FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                    int totalRows = workSheet.Dimension.Rows;
                    List<TravelInsuranceType> TravelInsuranceTypeList = new List<TravelInsuranceType>();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        TravelInsuranceTypeList.Add(new TravelInsuranceType
                        {
                            Id = Guid.NewGuid(),
                            Code = workSheet.Cells[i, 1].Value.ToString(),
                            Name = workSheet.Cells[i, 2].Value.ToString(),
                            Description = workSheet.Cells[i, 3].Value.ToString(),
                            Term = workSheet.Cells[i, 4].Value.ToString(),
                            CreatedDate = DateTime.Now,
                            CreatedBy = "HninYee",
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = "HninYee",
                            Active = true
                        });
                    }
                    _db.TravelInsuranceType.AddRange(TravelInsuranceTypeList);
                    _db.SaveChanges();
                    return RedirectToAction("TravelInsuranceType");
                }
            }
            
        }

        public IActionResult ExportTravelInsuranceTypeList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var travelInsuranceTypes = (from t1 in _db.TravelInsuranceType
                                            select new TravelInsuranceType
                                            {
                                                Name = t1.Name,
                                                Description = t1.Description,
                                                Term = t1.Term,
                                            }
                            ).ToList();


                var columnHeader = new List<string>
                {
                    "Code",
                    "Name",
                    "Descrption",
                    "Term",
                };

                var fileContent = ExportExcelForUserList(travelInsuranceTypes, columnHeader, "Test");
                return File(fileContent, "application/ms-excel", "TravelInsuranceTypeList.xlsx");
            }
        }

        public byte[] ExportExcelForUserList(List<TravelInsuranceType> travelInsuranceTypes, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in travelInsuranceTypes)
                {
                    worksheet.Cells["A" + j].Value = item.Code;
                    worksheet.Cells["B" + j].Value = item.Name;
                    worksheet.Cells["C" + j].Value = item.Description;
                    worksheet.Cells["D" + j].Value = item.Term;
                   
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        [Route("Delete")]
        public async Task<IActionResult> DeleteTravelInsuranceTypeAsync(Guid Id)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var travelType = await _db.TravelInsuranceType.AsNoTracking().SingleOrDefaultAsync(m => m.Id == Id);
                if (travelType == null)
                {
                    return RedirectToAction("TravelInsuranceType");
                }
                try
                {
                    _db.TravelInsuranceType.Remove(travelType);
                    await _db.SaveChangesAsync();
                    return RedirectToAction("TravelInsuranceType");
                }
                catch (DbUpdateException)
                {
                    return RedirectToAction("TravelInsuranceType");
                }
            }
        }

        [Route("Edit/{Id}")]
        public async Task<IActionResult> EditTravelInsuranceType(Guid Id)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                if (Id == null)
                {
                    return NotFound();
                }
                TravelInsuranceType travelType = await _db.TravelInsuranceType.Where(u => u.Id == Id).FirstOrDefaultAsync();
                return View("CreateTravelInsuranceType", travelType);
                //return View();
            }
        }

        #endregion

        #region Premium Unit Amount
        public IActionResult PremiumUnitAmountList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                PremiumUnitAmount premiumUnitAmount = new PremiumUnitAmount();
                ViewBag.PremiumUnitAmountList = (from t1 in _db.PremiumUnitAmount
                                                 join t2 in _db.TravelInsuranceType
                                                 on t1.TravelInsuranceTypeCode equals t2.Code
                                                 orderby t1.Days
                                                 select new PremiumUnitAmountViewModel
                                                 {
                                                     Id = t1.Id,
                                                     Name = t1.Name,
                                                     Description = t1.Description,
                                                     Days = t1.Days.ToString(),
                                                     Amount = t1.Amount,
                                                     TravelInsuranceTypeName = t2.Name
                                                     
                                                 }).ToList();


                return View();
            }
        }

        public IActionResult ImportPremiumUnitAmountList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                return View();
            }
        }

        [HttpPost]
        [Route("ImportPremiumUnitAmount")]
        public IActionResult ImportPremiumUnitAmountListExcelData(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                string folderName = "ImportedFiles";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                // Delete Files from Directory
                System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                foreach (FileInfo filesDelete in di.GetFiles())
                {
                    filesDelete.Delete();
                }// End Deleting files form directories

                if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                {
                    Directory.CreateDirectory(newPath);
                }
                var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                {
                    reportfile.CopyTo(fileStream);
                }
                // Get uploaded file path with root
                string rootFolder = _hostingEnvironment.WebRootPath;
                string fileName = @"ImportedFiles/" + fiName;
                FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                    int totalRows = workSheet.Dimension.Rows;
                    List<PremiumUnitAmount> premiumUnitAmountsList = new List<PremiumUnitAmount>();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        premiumUnitAmountsList.Add(new PremiumUnitAmount
                        {
                            Id = Guid.NewGuid(),

                            Name = workSheet.Cells[i, 1].Value.ToString(),
                            Description = workSheet.Cells[i, 2].Value.ToString(),
                            Days = Convert.ToInt32(workSheet.Cells[i, 3].Value),
                            Amount = Convert.ToDecimal(workSheet.Cells[i, 4].Value),
                            TravelInsuranceTypeCode = workSheet.Cells[i, 5].Value.ToString(),
                            TravelInsuranceTypeID = (from x in _db.TravelInsuranceType
                                                     where x.Code == workSheet.Cells[i, 5].Value.ToString()
                                                     select x.Id).FirstOrDefault(),
                            //TravelInsuranceTypeID = _db.TravelInsuranceType.Where(x => x.Code == workSheet.Cells[i, 6].Value.ToString()).Select(t1 => t1.Id).FirstOrDefault(),
                            CreatedDate = DateTime.Now,
                            CreatedBy = "HninYee",
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = "HninYee",
                        });
                    }
                    _db.PremiumUnitAmount.AddRange(premiumUnitAmountsList);
                    _db.SaveChanges();
                    return RedirectToAction("PremiumUnitAmountList");
                }
            }

        }

        public IActionResult ExportPremiumUnitAmountList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                PremiumUnitAmount premiumUnitAmount = new PremiumUnitAmount();
                var PremiumUnitAmountList = (from t1 in _db.PremiumUnitAmount
                                             join t2 in _db.TravelInsuranceType
                                             on t1.TravelInsuranceTypeCode equals t2.Code
                                             orderby t1.TravelInsuranceTypeCode
                                             select new PremiumUnitAmountViewModel
                                             {
                                                 Name = t1.Name,
                                                 Description = t1.Description,
                                                 Days = t1.Days.ToString(),
                                                 Amount = t1.Amount,
                                                 TravelInsuranceTypeName = t2.Name
                                             }).ToList();

                var columnHeader = new List<string>
            {
                "Name",
                "Descrption",
                "Days",
                "Amount",
                "TravelInsuranceType",
            };

                var fileContent = ExportExcelForUserList(PremiumUnitAmountList, columnHeader, "Test");
                return File(fileContent, "application/ms-excel", "PremiumUnitAmountList.xlsx");
            }
        }

        public byte[] ExportExcelForUserList(List<PremiumUnitAmountViewModel> premiumUnitAmountViewModels, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in premiumUnitAmountViewModels)
                {
                    worksheet.Cells["A" + j].Value = item.Name;
                    worksheet.Cells["B" + j].Value = item.Description;
                    worksheet.Cells["C" + j].Value = item.Days;
                    worksheet.Cells["D" + j].Value = item.Amount;
                    worksheet.Cells["E" + j].Value = item.TravelInsuranceTypeName;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        [Route("DeletePremiumUnitAmount")]
        public async Task<IActionResult> DeletePremiumUnitAmountAsync(Guid Id)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var premiumUnitAmount = await _db.PremiumUnitAmount.AsNoTracking().SingleOrDefaultAsync(m => m.Id == Id);
                if (premiumUnitAmount == null)
                {
                    return RedirectToAction("PremiumUnitAmountList");
                }
                try
                {
                    _db.PremiumUnitAmount.Remove(premiumUnitAmount);
                    await _db.SaveChangesAsync();
                    return RedirectToAction("PremiumUnitAmountList");
                }
                catch (DbUpdateException)
                {
                    return RedirectToAction("PremiumUnitAmountList");
                }
            }
        }

        [Route("EditPremiumUnitAmount/{Id}")]
        public async Task<IActionResult> EditPremiumUnitAmount(Guid Id)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                if (Id == null)
                {
                    return NotFound();
                }
                PremiumUnitAmount premiumUnitAmount = await _db.PremiumUnitAmount.Where(u => u.Id == Id).FirstOrDefaultAsync();
                ViewBag.TravelInsuranceType = new SelectList((from r in _db.TravelInsuranceType
                                                              select r).ToList(), "Code", "Name");

                return View("CreatePremiumUnitAmount", premiumUnitAmount);
            }
            //return View();
            // }
        }

        public IActionResult CreatePremiumUnitAmount(TravelInsuranceType travelInsuranceType)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                ViewBag.TravelInsuranceType = new SelectList((from r in _db.TravelInsuranceType
                                                              select r).ToList(), "Code", "Name");
                return View();
            }
        }

        [HttpPost]
        public IActionResult SavePremiumUnitAmount(PremiumUnitAmount model)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                try
                {
                    if (model.Id == Guid.Empty)
                    {
                        model.Id = Guid.NewGuid();
                        model.CreatedDate = DateTime.Now;
                        model.ModifiedDate = DateTime.Now;
                        session = HttpContext.Session;
                        string username = session.GetString("username");
                        model.CreatedBy = username;
                        model.ModifiedBy = username;

                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();

                        ViewBag.Message = "Save Successful.";
                        return RedirectToAction("PremiumUnitAmountList");
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            session = HttpContext.Session;
                            model.ModifiedBy = session.GetString("username");
                            model.ModifiedDate = DateTime.Now;

                            _db.Update(model);
                            _db.SaveChanges();

                            ViewBag.Message = "Save Successful.";
                        }
                    }
                }
                catch (DbUpdateException)
                {
                    TempData["Message"] = "Error in Saving Process.";
                    return RedirectToAction("CreatePremiumUnitAmount", model);
                    //throw ex;
                }
            }
            TempData["Message"] = "Save Successful";
            return RedirectToAction("PremiumUnitAmountList");
        }
        #endregion
    }
}