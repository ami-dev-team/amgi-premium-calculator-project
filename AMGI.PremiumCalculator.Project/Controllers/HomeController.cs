﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AMGI.PremiumCalculator.Project.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using AMGI.PremiumCalculator.Project.Services;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AMGI.PremiumCalculator.Project.Controllers
{
    public class HomeController : Controller
    {
        HttpClient client = null;
        public ISession session;
        private readonly IHttpContextAccessor _httpContextAccessor;
        MainDBContext _db = new MainDBContext();
        public IConfiguration Configuration { get; set; }

        public HomeController(IHttpContextAccessor httpContextAccessor)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            this._httpContextAccessor = httpContextAccessor;
            client = new HttpClient();
            client.BaseAddress = new Uri(Configuration["Middleware:IPAddress"].ToString());
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        public IActionResult Index()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            var UserEmail = session.GetString("email");

            ViewBag.TravelTypeList = new SelectList(_db.TravelInsuranceType.ToList(), "Name", "Name");
            return View();
        }

        public IActionResult TravelPremiumDetail()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            
            TravelPremiumUnitAmountViewModel dataModel = HttpContext.Session.GetObjectFromJson<TravelPremiumUnitAmountViewModel>("TravelCalculatorSession");
            return View(dataModel);
        }

        public async Task<IActionResult> CalculateTravel([FromBody] TravelReq model)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            try
            {
                TravelPremiumUnitAmountViewModel TravelCalculator = new TravelPremiumUnitAmountViewModel();
                var json = JsonConvert.SerializeObject(new { fromDate = model.DepartureDate, toDate = model.ArrivalDate, travelType = model.TravelType, unit = model.Unit });
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

                var message = await client.PostAsync($"/travel/GetPremiumUnitAmount", content);

                if (message.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    ViewBag.Message = "Premium Unit Amount can not calculate.";
                    return View(TravelCalculator);
                }
                else if (message.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    ViewBag.Message = "Policy number is not found.";
                    session.SetString("btnDisable", "false");
                    return View(TravelCalculator);
                }
                else
                {
                    message.EnsureSuccessStatusCode();
                  
                    if (message.IsSuccessStatusCode)
                    {
                        HttpContext.Session.SetObjectAsJson("TravelCalculatorSession", null);
                        string jsonData = await message.Content.ReadAsStringAsync();
                        TravelCalculator = JsonConvert.DeserializeObject<TravelPremiumUnitAmountViewModel>(jsonData);
                        HttpContext.Session.SetObjectAsJson("TravelCalculatorSession", TravelCalculator);
                    }
                    return Json(TravelCalculator);
                }
            }
            catch(Exception ex)
            {
                return View();
            }
        }
        
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
