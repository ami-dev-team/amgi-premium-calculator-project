﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AMGI.PremiumCalculator.Project.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;

namespace AMGI.PremiumCalculator.Project.Controllers
{
    public class SetupController : Controller
    {
        public ISession session;
        private readonly IHostingEnvironment _hostingEnvironment;

        public SetupController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }


        MainDBContext _db = new MainDBContext();

        #region Branch

        public IActionResult Branch()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                ViewBag.BranchList = _db.Branch.ToList();
                return View(new Branch());
            }
        }

        [HttpPost]
        public IActionResult Branch(Branch model)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                if (model == null || model.Name == null)
                {
                    TempData["Message"] = "Please enter the name.";
                }
                else
                {
                    if (model.BranchId == Guid.Empty)
                    {
                        model.BranchId = Guid.NewGuid();
                        model.Name = model.Name.ToUpper();
                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();
                        ViewBag.Message = "Save Successful.";
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            _db.Update(model);
                            _db.SaveChanges();
                            ViewBag.Message = "Update Successful.";
                        }
                        else
                        {
                            ViewBag.Message = "Error in Update Process.";
                        }
                    }
                }
            }
            ViewBag.BranchList = _db.Branch.ToList();
            return RedirectToAction("Branch");
        }

        public IActionResult EditBranch(Guid branchId)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                Branch branch = _db.Branch.Where(r => r.BranchId == branchId).FirstOrDefault();
                ViewBag.BranchList = _db.Branch.ToList();
                return View("Branch", branch);
            }
        }

        public IActionResult DeleteBranch(Guid branchId)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var branch = _db.Branch.AsNoTracking().SingleOrDefault(m => m.BranchId == branchId);
                if (branch == null)
                {
                    return RedirectToAction(nameof(Branch));
                }
                try
                {
                    var userList = _db.SysUser.Where(u => u.Branch == branchId).ToList();
                    if (userList.Count == 0)
                    {
                        _db.Branch.Remove(branch);
                        _db.SaveChanges();
                        return RedirectToAction("Branch", new Branch());
                    }
                    else
                    {
                        TempData["Message"] = "This branch has already used in process.";
                    }
                    ViewBag.BranchList = _db.Branch.ToList();
                    return RedirectToAction("Branch", null);

                }
                catch (DbUpdateException)
                {
                    return View("Branch", new { id = branchId, saveChangesError = true });
                }
            }

        }

        #endregion

        #region Department
        public IActionResult Department()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                ViewBag.DepartmentList = _db.Department.ToList();
                return View(new Department());
            }
        }

        [HttpPost]
        public IActionResult Department(Department model)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                if (model == null || model.Name == null)
                {
                    TempData["Message"] = "Please enter the name.";
                }
                else
                {
                    if (model.Id == Guid.Empty)
                    {
                        model.Id = Guid.NewGuid();
                        model.Name = model.Name.ToUpper();
                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();
                        ViewBag.Message = "Save Successful.";
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            _db.Update(model);
                            _db.SaveChanges();
                            ViewBag.Message = "Update Successful.";
                        }
                        else
                        {
                            ViewBag.Message = "Error in Update Process.";
                        }
                    }
                }
            }
            ViewBag.DepartmentList = _db.Department.ToList();
            return RedirectToAction("Department");
        }

        public IActionResult EditDepartment(Guid deptId)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                Department dept = _db.Department.Where(r => r.Id == deptId).FirstOrDefault();
                ViewBag.DepartmentList = _db.Department.ToList();
                return View("Department", dept);
            }
        }

        public IActionResult DeleteDepartment(Guid deptId)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var department = _db.Department.AsNoTracking().SingleOrDefault(m => m.Id == deptId);
                if (department == null)
                {
                    return RedirectToAction(nameof(Department));
                }
                try
                {
                    var userList = _db.SysUser.Where(u => u.Department == deptId).ToList();
                    if (userList.Count == 0)
                    {
                        _db.Department.Remove(department);
                        _db.SaveChanges();
                        return RedirectToAction("Department", new Department());
                    }
                    else
                    {
                        TempData["Message"] = "This department has already used in process.";
                    }
                    ViewBag.DepartmentList = _db.Department.ToList();
                    return RedirectToAction("Department", null);

                }
                catch (DbUpdateException)
                {
                    return View("Branch", new { id = deptId, saveChangesError = true });
                }
            }

        }

        #endregion

        #region Product

        public IActionResult Product()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                Product Product = new Product();
                var ProductList = _db.Product.ToList();
                ViewBag.ProductList = ProductList;
                return View();
            }
        }

        public IActionResult CreateProduct(Product product)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                return View();
            }
        }

        [HttpPost]
        public IActionResult SaveProduct(Product model)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                try
                {
                    if (model.Id == Guid.Empty)
                    {
                        model.Id = Guid.NewGuid();
                        model.CreatedDate = DateTime.Now;
                        model.ModifiedDate = DateTime.Now;
                        session = HttpContext.Session;
                        string createdby = session.GetString("username");
                        model.CreatedBy = createdby;
                        model.ModifiedBy = createdby;
                        model.Active = true;

                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();

                        ViewBag.Message = "Save Successful.";
                        return RedirectToAction("Product");
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            session = HttpContext.Session;
                            model.ModifiedBy = session.GetString("username");
                            model.ModifiedDate = DateTime.Now;

                            _db.Update(model);
                            _db.SaveChanges();

                            ViewBag.Message = "Save Successful.";
                        }
                    }
                }
                catch (DbUpdateException)
                {
                    TempData["Message"] = "Error in Saving Process.";
                    return RedirectToAction("CreateProduct", model);
                    //throw ex;
                }
            }
            TempData["Message"] = "Save Successful";
            return RedirectToAction("Product");
        }

        public IActionResult ImportProductList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                return View();
            }
        }

        [HttpPost]
        [Route("ImportProductList")]
        public IActionResult ImportExcelData(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                string folderName = "ImportedFiles";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                // Delete Files from Directory
                System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                foreach (FileInfo filesDelete in di.GetFiles())
                {
                    filesDelete.Delete();
                }// End Deleting files form directories

                if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                {
                    Directory.CreateDirectory(newPath);
                }
                var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                {
                    reportfile.CopyTo(fileStream);
                }
                session = HttpContext.Session;
                string username = session.GetString("username");
                // Get uploaded file path with root
                string rootFolder = _hostingEnvironment.WebRootPath;
                string fileName = @"ImportedFiles/" + fiName;
                FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                    int totalRows = workSheet.Dimension.Rows;
                    List<Product> ProductList = new List<Product>();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        ProductList.Add(new Product
                        {
                            Id = Guid.NewGuid(),
                            Name = workSheet.Cells[i, 1].Value.ToString(),
                            Description = workSheet.Cells[i, 2].Value.ToString(),
                            CreatedDate = DateTime.Now,
                            CreatedBy = username,
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = username,
                            Active = true
                        });
                    }
                    _db.Product.AddRange(ProductList);
                    _db.SaveChanges();
                    return RedirectToAction("Product");
                }
            }

        }

        public IActionResult ExportProductList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var ProductList = (from t1 in _db.Product
                                   select new Product
                                   {
                                       Name = t1.Name,
                                       Description = t1.Description,
                                   }
                            ).ToList();


                var columnHeader = new List<string>
            {
                "Name",
                "Descrption",
            };

                var fileContent = ExportExcelForUserList(ProductList, columnHeader, "Test");
                return File(fileContent, "application/ms-excel", "ProductList.xlsx");
            }
        }

        public byte[] ExportExcelForUserList(List<Product> product, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in product)
                {
                    worksheet.Cells["A" + j].Value = item.Name;
                    worksheet.Cells["B" + j].Value = item.Description;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        [Route("DeleteProduct")]
        public async Task<IActionResult> DeleteProductAsync(Guid Id)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var product = await _db.Product.AsNoTracking().SingleOrDefaultAsync(m => m.Id == Id);
                if (product == null)
                {
                    return RedirectToAction("Product");
                }
                try
                {
                    _db.Product.Remove(product);
                    await _db.SaveChangesAsync();
                    return RedirectToAction("Product");
                }
                catch (DbUpdateException)
                {
                    return RedirectToAction("Product");
                }
            }
        }

        [Route("EditProduct/{Id}")]
        public async Task<IActionResult> EditProduct(Guid Id)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                if (Id == null)
                {
                    return NotFound();
                }
                Product product = await _db.Product.Where(u => u.Id == Id).FirstOrDefaultAsync();
                return View("CreateProduct", product);
            }
        }

        #endregion
    }
}