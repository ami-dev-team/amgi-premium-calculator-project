﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using AMGI.PremiumCalculator.Project.Areas.Identity.Pages.Account;
using AMGI.PremiumCalculator.Project.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;

namespace AMGI.PremiumCalculator.Project.Controllers
{
    public class AdminController : Controller
    {
        MainDBContext _db = new MainDBContext();
        Common com = new Common();
        private IHostingEnvironment _env;
        public ISession session;

        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        //DataService dataservice = new DataService();

        public AdminController(IHostingEnvironment env, UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _env = env;
        }

        #region Role
        public IActionResult Role()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                ViewBag.RoleList = _db.Role.ToList();
                return View(new Role());
            }
        }

        [HttpPost]
        public IActionResult Role(Role model)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                if (model == null || model.RoleName == null)
                {
                    TempData["Message"] = "Please enter the name.";
                }
                else
                {
                    if (model.RoleId == Guid.Empty)
                    {
                        model.RoleId = Guid.NewGuid();
                        model.RoleName = model.RoleName.ToUpper();
                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();
                        ViewBag.Message = "Save Successful.";
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            _db.Update(model);
                            _db.SaveChanges();
                            ViewBag.Message = "Update Successful.";
                        }
                        else
                        {
                            ViewBag.Message = "Error in Update Process.";
                        }
                    }
                }
                ViewBag.RoleList = _db.Role.ToList();
                return RedirectToAction("Role");
            }
        }

        public IActionResult EditRole(Guid roleId)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                Role role = _db.Role.Where(r => r.RoleId == roleId).FirstOrDefault();
                ViewBag.RoleList = _db.Role.ToList();
                return View("Role", role);
            }
        }

        public IActionResult DeleteRole(Guid roleId)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var role = _db.Role.AsNoTracking().SingleOrDefault(m => m.RoleId == roleId);
                if (role == null)
                {
                    return RedirectToAction(nameof(Branch));
                }
                try
                {
                    var userList = _db.SysUser.Where(u => u.Role == roleId).ToList();
                    if (userList.Count == 0)
                    {
                        _db.Role.Remove(role);
                        _db.SaveChanges();
                        return RedirectToAction("Role", new Role());
                    }
                    else
                    {
                        TempData["Message"] = "This role has already used in process.";
                    }
                    ViewBag.RoleList = _db.Role.ToList();
                    return RedirectToAction("Role", null);
                }
                catch (DbUpdateException)
                {
                    return View("Role", new { id = roleId, saveChangesError = true });
                }
            }

        }
        #endregion

        #region User
        public IActionResult User()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                ViewBag.DeptList = new SelectList(_db.Department.ToList(), "Id", "Description");
                ViewBag.BranchList = new SelectList(_db.Branch.ToList(), "BranchId", "Description");
                ViewBag.RoleList = new SelectList(_db.Role.ToList(), "RoleId", "Description");
            }
            return View(new SysUser());
           // return View(new SysUser());
        }

        public IActionResult UserList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                ViewBag.UserList = (from t1 in _db.SysUser
                                join t2 in _db.Branch on t1.Branch equals t2.BranchId
                                join t3 in _db.Department on t1.Department equals t3.Id
                                join t4 in _db.Role on t1.Role equals t4.RoleId
                                    where t1.AccountStatus !="Lock"
                                select new UserInfo
                                {
                                    Id = t1.Id,
                                    Name = t1.Name,
                                    Role = t4.RoleName,
                                    NRC = t1.Nrc,
                                    Email = t1.Email,
                                    Department = t3.Description,
                                    Branch = t2.Description,
                                    Address = t1.Address
                                }).ToList();
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UserAsync(SysUser model)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                try
                {
                    if (model.Id == Guid.Empty)
                    {
                        string psalt = com.GenerateSalt(8);

                        model.Id = Guid.NewGuid();
                        model.CreatedDate = DateTime.Now;
                        model.PasswordSalt = psalt;
                        model.Password = com.EncryptPassword("12345678", model.PasswordSalt);
                        model.IsFirstTimeLogin = true;
                        model.IsActive = true;
                        session = HttpContext.Session;
                        string createdby = session.GetString("username");
                        model.CreatedBy = createdby;

                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();

                        if (ModelState.IsValid)
                        {
                            //return Redirect("~/Identity/Account/Register");
                            await CreateAspNetUsersAsync(model);
                        }

                        ViewBag.Message = "Save Successful.";
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            session = HttpContext.Session;
                            model.UpdatedBy = session.GetString("username");
                            model.UpdatedDate = DateTime.Now;

                            _db.Update(model);
                            _db.SaveChanges();

                            var email = _db.AspNetUser.Where(user => user.Email == model.Email).FirstOrDefault();
                            if (email == null)
                            {
                                // return Redirect("~/Identity/Account/Register");
                                await CreateAspNetUsersAsync(model);
                            }

                            ViewBag.Message = "Save Successful.";
                            return RedirectToAction("UserList");
                        }
                    }
                }
                catch (DbUpdateException)
                {
                    TempData["Message"] = "Error in Saving Process.";
                    return RedirectToAction("User", model);
                }
            }
            TempData["Message"] = "Save Successful";
            return RedirectToAction("UserList");
        }

        private async Task CreateAspNetUsersAsync(SysUser model)
        {
           
               if (ModelState.IsValid)
                {
                    var user = new IdentityUser { UserName = model.Email, Email = model.Email };
                    var result = await _userManager.CreateAsync(user, "Ami@123");
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("User created a new account with password.");

                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.Page(
                            "/Account/ConfirmEmail",
                            pageHandler: null,
                            values: new { userId = user.Id, code = code },
                            protocol: Request.Scheme);
                        //await _emailSender.SendEmailAsync(model.Email, "Confirm your email",
                        //    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                        await _emailSender.SendEmailAsync(model.Email, "Confirm your email",
                          $"Please confirm your account by <a href='#'>clicking here</a>.");


                        await _signInManager.SignInAsync(user, isPersistent: false);
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
           
        }
        
        public async Task<IActionResult> OnPostAsync(SysUser model,string returnUrl = null)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                returnUrl = returnUrl ?? Url.Content("~/");
                if (ModelState.IsValid)
                {
                    var user = new IdentityUser { UserName = model.Email, Email = model.Email };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("User created a new account with password.");

                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.Page(
                            "/Account/ConfirmEmail",
                            pageHandler: null,
                            values: new { userId = user.Id, code = code },
                            protocol: Request.Scheme);

                        //await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        //    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            // If we got this far, something failed, redisplay form
            //return Page();
            return RedirectToAction("UserList");
        }

        public IActionResult ExportUserList()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                var userInfos = (from t1 in _db.SysUser
                                 join t2 in _db.Role on t1.Role equals t2.RoleId
                                 join t3 in _db.Department on t1.Department equals t3.Id
                                 join t4 in _db.Branch on t1.Branch equals t4.BranchId
                                 select new UserInfo
                                 {
                                     Name = t1.Name,
                                     Role = t2.RoleName,
                                     Email = t1.Email,
                                     Phone = t1.Phone,
                                     Department = t3.Description,
                                     Branch = t4.Description,
                                     Address = t1.Address

                                 }).ToList();

                var columnHeader = new List<string>
            {
                "Name",
                "Role",
                "Email",
                "Phone",
                "Department",
                "Branch",
                "Address"
            };

                var fileContent = ExportExcelForUserList(userInfos, columnHeader, "Test");
                return File(fileContent, "application/ms-excel", "UserList.xlsx");
            }
            //return View();
        }

        private byte[] ExportExcelForUserList(List<UserInfo> userList, List<string> columnHeader, string heading)
        {
           
                byte[] result = null;
                using (ExcelPackage package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add(heading);
                    worksheet.DefaultColWidth = 20;
                    using (var cells = worksheet.Cells[1, 1, 1, 8])
                    {
                        cells.Style.Font.Bold = true;
                    }

                    for (int i = 0; i < columnHeader.Count(); i++)
                    {
                        worksheet.Cells[1, i + 1].Value = columnHeader[i];
                    }
                    var j = 2;
                    var count = 1;
                    foreach (var item in userList)
                    {
                        worksheet.Cells["A" + j].Value = item.Name;
                        worksheet.Cells["B" + j].Value = item.Role;
                        worksheet.Cells["C" + j].Value = item.Email;
                        worksheet.Cells["D" + j].Value = item.Phone;
                        worksheet.Cells["E" + j].Value = item.Department;
                        worksheet.Cells["F" + j].Value = item.Branch;
                        worksheet.Cells["G" + j].Value = item.Address;

                        j++;
                        count++;
                    }
                    result = package.GetAsByteArray();
                }
                return result;
            
        }

        [Route("CheckInfo/{userId}")]
        public IActionResult CheckUserInfo(Guid userId)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                if (userId == Guid.Empty)
                {
                    ViewBag.Flag = "true";
                }

                SysUser user = _db.SysUser.Where(r => r.Id == userId).FirstOrDefault();
                ViewBag.RoleList = new SelectList(_db.Role.ToList(), "RoleId", "Description");
                return View(user);
            }
        }

        public IActionResult DeleteUser(Guid Id)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                SysUser sysUser = _db.SysUser.Where(u => u.Id == Id).FirstOrDefault();
                if (sysUser != null)
                {
                    AspNetUsers user = _db.AspNetUser.Where(u => u.Email == sysUser.Email).FirstOrDefault();

                    _db.SysUser.Remove(sysUser);
                    _db.AspNetUser.Remove(user);
                    _db.SaveChanges();

                }
                return RedirectToAction("UserList");
            }
        }

        public IActionResult EditUser(Guid UserId)
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                ViewBag.RoleList = new SelectList(_db.Role.ToList(), "RoleId", "Description");
            ViewBag.BranchList = new SelectList(_db.Branch.ToList(), "BranchId", "Description");
            ViewBag.DeptList = new SelectList(_db.Department.ToList(), "Id", "Description");

            var sysUser = _db.SysUser.Where(u => u.Id == UserId).FirstOrDefault();

            return View("User", sysUser);
            }
        }

        #endregion
    }
}