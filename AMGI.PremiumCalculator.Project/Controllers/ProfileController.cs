﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMGI.PremiumCalculator.Project.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AMGI.PremiumCalculator.Project.Controllers
{
    public class ProfileController : Controller
    {
        MainDBContext _db = new MainDBContext();
        public ISession session;
        public IActionResult Index()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                return View();
            }
        }

        public IActionResult Detail()
        {
            session = HttpContext.Session;
            if (string.IsNullOrEmpty(session.GetString("username")))
                return RedirectToAction("Login", "Account");
            else
            {
                Guid userid = new Guid(session.GetString("userId"));

                UserInfo userinfo = (from t1 in _db.SysUser
                                     join t2 in _db.Department on t1.Department equals t2.Id
                                     join t3 in _db.Branch on t1.Branch equals t3.BranchId
                                     join t4 in _db.Role on t1.Role equals t4.RoleId
                                     where t1.Id == userid
                                     select new UserInfo
                                     {
                                         Name = t1.Name,
                                         NRC = t1.Nrc,
                                         Email = t1.Email,
                                         Phone = t1.Phone,
                                         Address = t1.Address,
                                         Role = t4.RoleName,
                                         Branch = t3.Description,
                                         Department = t2.Description,
                                         IsActive = t1.IsActive
                                     }
                                     ).FirstOrDefault();
                return View(userinfo);
            }

        }
    }
}